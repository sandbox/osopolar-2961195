# Helper module to add GROUP_CONCAT aggregate function

*as suggested in [Post #72 of the issue: **Add GROUP_CONCAT aggregate function...**](https://www.drupal.org/project/views/issues/1362524#comment-11855075).*

Without the module the `group_concat` aggregation in the views query won't work or will require patching views.